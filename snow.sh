# usage Artifact Name is the first input, and version is 2nd input
Artifact_registration() {
    echo "sending Artifact registration API call to  Servicenow."
    API_ENDPOINT="$SnowInstance/api/sn_devops/v1/devops/artifact/registration?orchestrationToolId=$OrchToolID"
    REQUEST_DATA="{
      \"artifacts\":[ 
        {
          \"name\":\"$1\",
          \"version\":\"$2.${CI_PIPELINE_ID}\",
          \"semanticVersion\":\"$2.0.${CI_PIPELINE_ID}\",
          \"repositoryName\":\"${CI_PROJECT_TITLE}\"
        }
        ],
          \"pipelineName\":\"$PipelineName\",
          \"stageName\":\"${CI_JOB_NAME}\",
          \"taskExecutionNumber\":\"${CI_JOB_ID}\",
          \"branchName\":\"${CI_COMMIT_BRANCH}\"
    }"
    RESPONSE=$(curl -X POST -H "Content-Type: application/json" -d "$REQUEST_DATA" -u "$USERNAME:$PASSWORD" "$API_ENDPOINT")
    echo "API REQUEST_DATA: $REQUEST_DATA"
    echo "API Response: $RESPONSE"
}

packageRegistration(){
    echo "sending Package registration API call to  Servicenow."
    API_ENDPOINT="$SnowInstance/api/sn_devops/v1/devops/package/registration?orchestrationToolId=$OrchToolID"
    REQUEST_DATA="{
      \"name\":\"$1\",
      \"artifacts\":[
        {
          \"name\":\"$2\",
          \"version\":\"$3.${CI_PIPELINE_ID}\",
          \"semanticVersion\":\"$3.${CI_PIPELINE_ID}.0\",
          \"repositoryName\":\"${CI_PROJECT_TITLE}\"
        }
          ],
    \"pipelineName\":\"$PipelineName\",
    \"stageName\":\"${CI_JOB_NAME}\",
    \"taskExecutionNumber\":\"${CI_JOB_ID}\",
    \"branchName\":\"${CI_COMMIT_BRANCH}\"
    }"
    RESPONSE=$(curl -X POST -H "Content-Type: application/json" -d "$REQUEST_DATA" -u "$USERNAME:$PASSWORD" "$API_ENDPOINT")
    echo "APIREQUEST_DATA: $REQUEST_DATA"
    echo "API Response: $RESPONSE"
}

SendUnitTest(){
    echo "Running unit tests... This will take about 60 seconds."
    API_ENDPOINT="$SnowInstance/api/sn_devops/v1/devops/tool/test?orchestrationToolId=$OrchToolID&toolId=$TestingToolID"
    REQUEST_DATA="{
      \"name\":\"$1.${CI_PIPELINE_ID}.0\",
      \"duration\":80.0,
      \"passedTests\":$3,
      \"failedTests\":$4,
      \"skippedTests\":$5,
      \"blockedTests\":$6,
      \"totalTests\":$2,
      \"startTime\":\"$(date '+%Y-%m-%dT%H:%M:%SZ')\",
      \"finishTime\":\"$(date '+%Y-%m-%dT%H:%M:%SZ')\",
      \"buildNumber\":\"${CI_JOB_ID}\", 
      \"stageName\":\"${CI_JOB_NAME}\", 
      \"pipelineName\":\"$PipelineName\",
      \"branchName\":\"${CI_COMMIT_BRANCH}\" 
    }"
    RESPONSE=$(curl -X POST -H "Content-Type: application/json" -d "$REQUEST_DATA" -u "$USERNAME:$PASSWORD" "$API_ENDPOINT")
    echo "API REQUEST_DATA: $REQUEST_DATA"
    echo "API Response: $RESPONSE"
}

sendSonarScan(){
    echo "sending SonarQube software quality registration scanss"
    cat $1
    #task_id=$(cat $1 | grep "More about the report processing at" | sed -n "/.*More about the report processing at /s/.*More about the report processing at //p")
    #var1=$(cat $1 | grep "you can find the results at:" | sed -n "/.*you can find the results at: /s/.*you can find the results at: //p")
    var1=$(cat $1  | egrep -o '[^ ]+\/dashboard\?[^ ]+')
    #echo "$task_id"
    echo "$var1"
    echo "$SONAR_HOST_URL"
    API_ENDPOINT="$SnowInstance/api/sn_devops/v1/devops/tool/softwarequality?orchestrationToolId=$OrchToolID&toolId=$SonarQubeToolID"
    REQUEST_DATA="{
      \"projectName\":\"${CI_PROJECT_TITLE}\",
      \"url\":\"$SONAR_HOST_URL\",
      \"resultsUrl\":\"$var1\",
      \"IdUrl\":\"$var1\",
      \"buildNumber\":\"${CI_JOB_ID}\",
      \"stageName\":\"${CI_JOB_NAME}\",
      \"pipelineName\":\"$PipelineName\",
      \"branchName\":\"${CI_COMMIT_BRANCH}\"
    }"
    RESPONSE=$(curl -X POST -H "Content-Type: application/json" -d "$REQUEST_DATA" -u "$USERNAME:$PASSWORD" "$API_ENDPOINT")
    echo "API REQUEST_DATA: $REQUEST_DATA"
    echo "API Response: $RESPONSE"
}

SendchangeAdditionalInfo()
{
    echo "sending Change Additional Info"
    API_ENDPOINT="$SnowInstance/api/sn_devops/v1/devops/additional_info/${CI_PIPELINE_ID}"
    REQUEST_DATA="{
    \"service_offering\":\"$1\",
    \"u_application_service\":\"$2\",
    \"recovery_duration\":\"$3\",
    \"additional_info\":{
    \"var1\":\"world123\"
        }
    }"
    RESPONSE=$(curl -X POST -H "Content-Type: application/json" -d "$REQUEST_DATA" -u "$USERNAME:$PASSWORD" "$API_ENDPOINT")
    echo "API REQUEST_DATA: $REQUEST_DATA"
    echo "API Response: $RESPONSE"
}